<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    /**
     * Don't auto-apply mass assignment protection.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The relationships to always eager-load.
     *
     * @var array
     */
    protected $with = ['category'];

    /**
     * Boot the model.
     */
    protected static function boot()
    {
        parent::boot();

        static::created(function ($package) {
            $package->update(['slug' => $package->name]);
        });
    }

    /**
     * Get the route key name.
     *
     * @return string
     */
    /*public function getRouteKeyName()
    {
        return 'slug';
    }*/

    /**
     * Get a string path for the thread.
     *
     * @return string
     */
    public function path()
    {
        return "/packages/{$this->category->slug}/{$this->slug}";
    }

    /**
     * A package is assigned a package category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
    	return $this->belongsTo(PackageCategory::class, 'category_id');
    }

    /**
     * Set the proper slug attribute.
     *
     * @param string $value
     */
    public function setSlugAttribute($value)
    {
        if (static::whereSlug($slug = str_slug($value))->exists()) {
            $slug = "{$slug}-" . $this->created_at->timestamp;
        }
        $this->attributes['slug'] = $slug;
    }
}
