<?php

namespace App\Models;

use App\Traits\Permissions\HasPermissionsTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasPermissionsTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Check is the current logged in user is admin.
     *
     * @return boolean
     */
    public function isAdmin()
    {
        if ($this->hasRole('super-admin') || $this->hasRole('admin')) {
            return true;
        }
        return false;
    }

    /**
     * Check is the current logged in user is super admin.
     *
     * @return boolean
     */
    public function isSuperAdmin()
    {
        if ($this->hasRole('super-admin')) {
            return true;
        }
        return false;
    }
}
