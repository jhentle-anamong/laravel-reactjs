<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Package;
use Illuminate\Http\Request;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::latest()
            ->if (request()->get('name'), 'name', 'LIKE', '%'.request()->get('name').'%')
            ->if (request()->get('status'), 'status', '=', request()->get('status'))
            ->if (request()->get('risk_profile'), 'risk_profile', '=', request()->get('risk_profile'))
            ->paginate(4);

        if (request()->wantsJson()) {
            return response()->json($packages);
        }

        return view('admin.packages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = request()->validate([
            'name'              => 'required',
            'amount'            => 'required|numeric',
            'fund_status'       => 'required|numeric',
            'duration'          => 'required|numeric',
            'status'            => 'required',
            'for_membership'    => 'required|boolean',
            'risk_profile'      => 'required',
            'frequency'         => 'required',
            'category_id'       => 'required|exists:package_categories,id'
        ]);

        $package = Package::create($data);

        if (request()->wantsJson()) {
            return response($package->load('category'), 201);
        }

        return response()->json('Package Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        return response()->json($package);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {
        $data = $request->validate([
            'name'              => 'required',
            'amount'            => 'required|numeric',
            'duration'          => 'required|numeric',
            'status'            => 'required',
            'for_membership'    => 'required|boolean',
            'risk_profile'      => 'required',
            'frequency'         => 'required',
            'category_id'       => 'required|exists:package_categories,id',
            'status'            => 'required',
        ]);

        $package->update($data);

        return response()->json('Package Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        $package->delete();

        return response()->json('Package Deleted Successfully.');
    }
}
