<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Package;
use Illuminate\Http\Request;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::latest()->paginate(4);

        if (request()->wantsJson()) {
            return response()->json($packages);
        }

        return view('member.packages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        if (request()->wantsJson()) {
            return response($package->load('category'), 201);
        }

        return view('member.packages.show');
    }
}
