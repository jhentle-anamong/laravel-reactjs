let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/**
 * App Dashboard Styles
 */
mix
.sass('resources/assets/sass/app.scss', 'public/css')
.styles(['node_modules/pace-js/themes/black/pace-theme-corner-indicator.css'], 'public/css/vendors.css');

/**
 * App Dashboard Scripts
 */

mix
.react('resources/assets/js/app.js', 'public/js')
.scripts([
    'node_modules/paper-dashboard/assets/js/bootstrap-checkbox-radio.js',
    'node_modules/paper-dashboard/assets/js/chartist.min.js',
    'node_modules/paper-dashboard/assets/js/bootstrap-notify.js',
    'node_modules/paper-dashboard/assets/js/paper-dashboard.js',
    'node_modules/pace-js/pace.min.js'
], 'public/js/dashboard-theme.js');

/**
 * Member Dashboard Styles
 */
mix
.sass('resources/assets/member/sass/app.scss', 'public/member/css')
.styles(['node_modules/pace-js/themes/black/pace-theme-corner-indicator.css'], 'public/member/css/vendors.css');

/**
 * Member Dashboard Scripts
 */
mix
.react('resources/assets/member/js/app.js', 'public/member/js')
.scripts([
    'node_modules/paper-dashboard/assets/js/bootstrap-checkbox-radio.js',
    'node_modules/paper-dashboard/assets/js/chartist.min.js',
    'node_modules/paper-dashboard/assets/js/bootstrap-notify.js',
    'node_modules/paper-dashboard/assets/js/paper-dashboard.js',
    'node_modules/pace-js/pace.min.js'
], 'public/member/js/dashboard-theme.js');

/**
 * Members Scripts & Styles
 */
mix
.sass('resources/assets/member/sass/package/package.scss', 'public/member/css');

mix
.react('resources/assets/member/js/packages.js', 'public/member/js');


/**
 * Admin Dashboard Styles
 */
mix
.sass('resources/assets/admin/sass/app.scss', 'public/admin/css')
.styles(
    [
        'node_modules/pace-js/themes/black/pace-theme-corner-indicator.css',
    ],
    'public/admin/css/vendors.css');

/**
 * Admin Dashboard Scripts
 */
mix
.react('resources/assets/admin/js/app.js', 'public/admin/js')
.scripts([
    'node_modules/paper-dashboard/assets/js/bootstrap-checkbox-radio.js',
    'node_modules/paper-dashboard/assets/js/chartist.min.js',
    'node_modules/paper-dashboard/assets/js/bootstrap-notify.js',
    'node_modules/paper-dashboard/assets/js/paper-dashboard.js',
    'node_modules/pace-js/pace.min.js'
], 'public/admin/js/dashboard-theme.js');

/**
 * Packages Scripts & Styles
 */
mix
.sass('resources/assets/admin/sass/package/package.scss', 'public/admin/css');

mix
.react('resources/assets/admin/js/packages.js', 'public/admin/js');

/**
 * Compress styles and scripts on run production
 */
if (mix.inProduction()) {
    mix.version();
}

/**
 * Browsersync Reloading
 */
mix.browserSync('laravel-reactjs.test');

