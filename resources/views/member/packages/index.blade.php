@extends('member.layouts.master')

@section('title', 'Packages')

@push('styles')
    <link rel="stylesheet" href="{{ mix('/member/css/package.css') }}">
@endpush

@section('content')
    <div id="packages-root"></div>
@endsection

@push('scripts')
    <script src="{{ mix('/member/js/packages.js') }}" defer></script>
@endpush

