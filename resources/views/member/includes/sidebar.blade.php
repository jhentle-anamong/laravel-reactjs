<div class="sidebar" data-background-color="black" data-active-color="danger">

<!--
    Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
    Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
-->

    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="" class="simple-text">
                {{ auth()->user()->name }}
            </a>
        </div>

        <ul class="nav">
            <li class="{{ Route::is('member.dashboard') ? 'active' : '' }}">
                <a href="{{ route('member.dashboard') }}">
                    <i class="ti-panel"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="{{ Route::is('member.packages.index') ? 'active' : '' }}">
                <a href="{{ route('member.packages.index') }}">
                    <i class="ti-package"></i>
                    <p>Packages</p>
                </a>
            </li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="ti-power-off"></i>
                    <p>Logout</p>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</div>
