<div class="sidebar" data-background-color="black" data-active-color="danger">

    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="{{ route('admin.profile', auth()->user()->id) }}" class="simple-text">
                {{ auth()->user()->name }}
            </a>
        </div>

        <ul class="nav">
            <li class="{{ Route::is('admin.dashboard') ? 'active' : '' }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="ti-panel"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            {{-- <li class="{{ Route::is('admin.profile') ? 'active' : '' }}">
                <a href="{{ route('admin.profile', auth()->user()->id) }}">
                    <i class="ti-user"></i>
                    <p>User Profile</p>
                </a>
            </li> --}}
            @role('super-admin')
                <li class="{{ Route::is('packages.index') ? 'active' : '' }}">
                    <a href="{{ route('packages.index') }}">
                        <i class="ti-package"></i>
                        <p>Packages</p>
                    </a>
                </li>
            @endrole
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="ti-power-off"></i>
                    <p>Logout</p>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</div>
