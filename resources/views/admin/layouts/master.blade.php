<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', config('app.name', 'Laravel'))</title>

    <!-- Bootstrap core CSS -->
    <!-- Animation library for notifications   -->
    <!--  Paper Dashboard core CSS    -->
    <link rel="stylesheet" href="{{ mix('/admin/css/app.css') }}">
    {{-- Css Vendors --}}
    <link rel="stylesheet" href="{{ mix('/admin/css/vendors.css') }}">

    @stack('styles')
</head>
<body>

<div class="wrapper">
    <div id="loader" class="loader">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>

    @include('admin.includes.sidebar')

    <div class="main-panel">
        @include('admin.includes.top-bar')

        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>

        @include('admin.includes.footer')
    </div>
</div>

</body>
    <!-- Scripts -->
    <script src="{{ mix('/admin/js/app.js') }}" defer></script>
    <script src="{{ mix('/admin/js/dashboard-theme.js') }}" defer></script>

    @stack('scripts')
</html>
