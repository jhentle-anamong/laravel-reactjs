@extends('admin.layouts.master')

@section('title', 'Packages')

@push('styles')
    <link rel="stylesheet" href="{{ mix('/admin/css/package.css') }}">
@endpush

@section('content')
	<div id="packages-root"></div>
@endsection

@push('scripts')
	<script src="{{ mix('/admin/js/packages.js') }}" defer></script>
@endpush

