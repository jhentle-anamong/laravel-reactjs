@extends('layouts.master')

@section('title', 'Register')

@push('styles')

@endpush

@section('content')
    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle navbar-toggle-black" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar "></span>
                    <span class="icon-bar "></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                       <a href="{{ route('login') }}" class="btn">Looking to login?</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="wrapper wrapper-full-page">
        <div class="register-page">
        <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="header-text">
                                <h2>Laravel React JS</h2>
                                <h4>Register for free and experience the dashboard today.</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-2">
                            <div class="media">
                                <div class="media-left">
                                    <div class="icon icon-danger">
                                        <i class="ti ti-user"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h5>Free Account</h5>
                                    Here you can write a feature description for your dashboard, let the users know what is the value that you give them.
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <div class="icon icon-warning">
                                        <i class="ti-bar-chart-alt"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h5>Awesome Performances</h5>
                                    Here you can write a feature description for your dashboard, let the users know what is the value that you give them.
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <div class="icon icon-info">
                                        <i class="ti-headphone"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h5>Global Support</h5>
                                    Here you can write a feature description for your dashboard, let the users know what is the value that you give them.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="card card-plain">
                                    <div class="content">

                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="email">Email address</label>
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="password-confirm">Confirm Password</label>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                        </div>
                                    </div>
                                    <div class="footer text-center">
                                        <button type="submit" class="btn btn-fill btn-danger btn-wd">Create Free Account</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer footer-transparent">
                <div class="container">
                    <div class="copyright text-center">
                        © <script async="" src="//www.google-analytics.com/analytics.js"></script><script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by Jhentle Anamong
                    </div>
                </div>
            </footer>
        </div>
    </div>

@endsection

@push('scripts')

@endpush
