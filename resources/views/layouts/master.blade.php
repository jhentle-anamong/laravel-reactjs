<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', config('app.name', 'Laravel React JS'))</title>

    <!-- Bootstrap core CSS -->
    <!-- Animation library for notifications   -->
    <!--  Paper Dashboard core CSS    -->
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    {{-- Css Vendors --}}
    <link rel="stylesheet" href="{{ mix('/css/vendors.css') }}">

    @stack('styles')
</head>
<body>
    @yield('content')
</body>
    <!-- Scripts -->
    <script src="{{ mix('/js/app.js') }}" defer></script>
    <script src="{{ mix('/js/dashboard-theme.js') }}" defer></script>

    @stack('scripts')
</html>
