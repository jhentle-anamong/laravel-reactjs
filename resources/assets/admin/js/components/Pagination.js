import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Pagination extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pagination: {},
            offset: 2
        };

        // this.pagesNumber    = this.pagesNumber.bind(this);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if ( nextProps.pagination ) {
            return {
                pagination: nextProps.pagination
            }
        }

        // No state update necessary
        return null;
    }

    pagesNumber() {
        const { pagination, offset } = this.state;

        if (!pagination.to) {
            return [];
        }

        let from = pagination.current_page - offset;

        if (from < 1) {
            from = 1;
        }

        let to = from + (offset * 2);

        if (to >= pagination.last_page) {
            to = pagination.last_page;
        }

        let pagesArray = [];

        for (let page = from; page <= to; page++) {
            pagesArray.push(page);
        }

        return pagesArray;
    }

    changePage(page, e) {
        e.preventDefault;
        this.props.paginate(page);
    }

    prevPage(e) {
        e.preventDefault;
        let page = this.state.pagination.current_page - 1;
        this.props.paginate(page);
    }

    nextPage(e) {
        e.preventDefault;
        let page = this.state.pagination.current_page + 1;
        this.props.paginate(page);
    }

    renderPages() {
        let pages = this.pagesNumber()

        return pages.map(page => {
            return (
             <li key={page} className={this.state.pagination.current_page == page ? 'active' : ''}>
                <a href="#" onClick={(e) => this.changePage(page, e)}>{ page }</a>
            </li>
            )
        });

    }


    render() {
        const { pagination } = this.state;

        if (pagination.total < pagination.per_page) {
            return null;
        }

        return (
            <ul className="pagination">
                <li className={pagination.current_page <= 1 ? 'disabled' : ''}>
                    <a href="#" onClick={(e) => this.prevPage(e)}><i className="fa fa-angle-double-left" aria-hidden="true"></i></a>
                </li>

                { this.renderPages() }

                <li className={pagination.current_page >= pagination.last_page ? 'disabled' : ''}>
                    <a href="#" onClick={(e) => this.nextPage(e)}><i className="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
        );

    }
}

Pagination.propTypes = {
    pagination: PropTypes.object.isRequired,
    paginate: PropTypes.func.isRequired
}


export default Pagination;
