import React, { Component } from 'react';

class Modal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: 'modal'
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        // Store prevItemId in state so we can compare when props change.
        // Clear out any previously-loaded user data (so we don't render stale stuff).
        if ( nextProps.id ) {
            return {
                id: nextProps.id
            }
        }

        // No state update necessary
        return null;
    }

    onClose(e) {
        console.log('Modal Closed');
    }

    onKeyUp(e) {
        // Lookout for ESC key (27)
        if (e.which === 27 && this.props.show) {
            this.onClose(e);
        }
    }

    componentDidMount() {
        document.addEventListener('keyup', this.onKeyUp);
    }

    componentWillUnmount() {
        document.removeEventListener('key', this.onKeyUp);
    }

    render() {
        return (
            <div id={this.state.id} className="modal fade">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 className="modal-title">{ this.props.title }</h4>
                        </div>
                        <div className="modal-body">
                            { this.props.children }
                        </div>
                        {/*<div className="modal-footer">
                            <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary">Save changes</button>
                        </div>*/}
                    </div>
                </div>
            </div>
        )
    }
}

// Modal.propTypes = {
//     onClose: PropTypes.func.isRequired
// }

export default Modal;