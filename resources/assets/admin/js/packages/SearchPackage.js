import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SearchPackage extends Component {
    constructor(props) {
        /* Initialize the state */
        super(props);

        this.state = {
            filter : {
                name: '',
                status: '',
                risk_profile: ''
            }
        };

        this.handleSubmit   = this.handleSubmit.bind(this);
        this.handleInput    = this.handleInput.bind(this);
    }


    /**
     * This method dynamically accepts inputs and stores it in the state
     */
    handleInput(key, e) {
        // Duplicating and updating the state
        const state = Object.assign({}, this.state.filter);
        state[key]  = e.target.value;
        this.setState({ filter: state });
    }

    /**
     * This method invoked when submit button is pressed
     */
    handleSubmit(e) {
        // preventDefault prevents page reload
        e.preventDefault();

        this.props.filter(this.state.filter);
    }


    render() {
        return (
            <div className="card">
                <div className="header">
                    <h4 className="title"><strong>Search Filter</strong></h4>
                </div>
                <div className="content">
                    <form onSubmit={this.handleSubmit}>
                        <div className="row">
                            <div className="col-sm-4 col-xs-12">
                                <div className="form-group">
                                    <label htmlFor="name">Name</label>
                                    <input type="text" name="name" className="form-control border-input" onChange={ (e) => this.handleInput('name', e) } />
                                </div>
                            </div>
                            <div className="col-sm-4 col-xs-12">
                                <div className="form-group">
                                    <label htmlFor="status">Status</label>
                                    <select name="status" className="form-control border-input" onChange={ (e) => this.handleInput('status', e)} >
                                        <option value="">All</option>
                                        <option value="available">Available</option>
                                        <option value="funded-active">Funded Active</option>
                                        <option value="closed">Closed</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-4 col-xs-12">
                                <div className="form-group">
                                    <label htmlFor="risk_profile">Risk Profile</label>
                                    <select name="risk_profile" className="form-control border-input" onChange={ (e) => this.handleInput('risk_profile', e)} >
                                        <option value="">All</option>
                                        <option value="conservative">Conservative</option>
                                        <option value="moderate">Moderate</option>
                                        <option value="aggressive">Aggressive</option>
                                        <option value="risky">Risky</option>
                                        <option value="high-risk">High-risk</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12">
                            <button type="submit" className="btn btn-info btn-fill btn-wd pull-right">Filter</button>
                        </div>
                        <div className="clearfix"></div>
                    </form>
                </div>
            </div>
        )
    }
}

SearchPackage.propTypes = {
    filter: PropTypes.func.isRequired
}


export default SearchPackage;
