import React, { Component } from 'react';
import Modal from '../components/Modal';

class AddPackage extends Component {
    constructor(props) {
        /* Initialize the state */
        super(props);

        this.state = {
            modal: {
                id: 'add-package',
                title: 'Add Package'
            },
            newPackage: {
                name: '',
                category_id: null,
                amount: null,
                fund_status: 0,
                status: '',
                duration: null,
                for_membership: 0,
                risk_profile: '',
                frequency: '',
            },
            errors: {}
        };

        // Boilerplate code for binding methods with 'this'
        this.handleSubmit   = this.handleSubmit.bind(this);
        this.handleInput    = this.handleInput.bind(this);
    }

    /**
     * This method dynamically accepts inputs and stores it in the state
     */
    handleInput(key, e) {
        // Duplicating and updating the state
        const state = Object.assign({}, this.state.newPackage);
        // const key   = e.target.name;
        state[key]  = e.target.value;
        this.setState({ newPackage: state },
            () => this.clearErrors(key));
    }

    /**
     * This method invoked when submit button is pressed
     */
    handleSubmit(e) {
        // preventDefault prevents page reload
        e.preventDefault();

        axios.post('/administrator/packages', this.state.newPackage)
        .then((response) => {
            /**
             * A call back to the onAdd props. The current state
             * is passed as a param
             */
            this.props.onAdd(response.data);

            console.log(response.data);

            $('#add-package').modal('hide');
        })
        .catch((error) => {
            this.setState({ errors: error.response.data.errors });
        });
    }

    /**
     * Determine if an errors exists for the given field.
     *
     * @param {string} field
     */
    hasError(field) {
        return (this.state.errors.hasOwnProperty(field) ? 'has-error' : '');
    }

    /**
     * Retrieve the error message for a field.
     *
     * @param {string} field
     */
    getError(field) {
        if (this.state.errors[field]) {
            return this.state.errors[field][0];
        }
    }

    /**
     * This method clear form fields
     */
    clearErrors(field){
        if (field) {
            const errors = this.state.errors;

            delete errors[field];

            this.setState({ errors: errors });

            return;
        }

        this.setState({ errors: {} })
    }

    render() {
        return (
            <Modal id={this.state.modal.id} title={this.state.modal.title}>
                <form onSubmit={ this.handleSubmit }>
                    {/* On every keystoke the handleInput method is invoked */}

                    <div className={`form-group ${this.hasError('name')}`}>
                        <label htmlFor="name">Package Name <span className="required">*</span></label>
                        <input type="text" name="name" className="form-control border-input" onChange={ (e) => this.handleInput('name', e) } />

                        <span className="help-block">{this.getError('name')}</span>
                    </div>

                    <div className={`form-group ${this.hasError('category_id')}`}>
                        <label htmlFor="category_id">Category <span className="required">*</span></label>
                        <select id="category_id" name="category_id" className="form-control border-input" onChange={ (e) => this.handleInput('category_id', e) }>
                            <option value="">Select Status</option>
                            <option value="1">Crypto Trading</option>
                            <option value="2">Crypto Mining</option>
                            <option value="3">Crypto Exchange</option>
                        </select>
                        <span className="help-block">{this.getError('category_id')}</span>
                    </div>

                    <div className={`form-group ${this.hasError('status')}`}>
                        <label htmlFor="status">Status <span className="required">*</span></label>
                        <select id="status" name="status" className="form-control border-input" onChange={ (e) => this.handleInput('status', e) }>
                            <option value="">Select Status</option>
                            <option value="available">Available</option>
                            <option value="funded-active">Funded Active</option>
                            <option value="closed">Closed</option>
                        </select>
                        <span className="help-block">{this.getError('status')}</span>
                    </div>

                    <div className={`form-group ${this.hasError('for_membership')}`}>
                        <label htmlFor="for_membership">For Membership Package Only <span className="required">*</span></label>
                        <select id="for_membership" name="for_membership" className="form-control border-input" onChange={ (e) => this.handleInput('for_membership', e) }>
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                        <span className="help-block">{this.getError('for_membership')}</span>
                    </div>

                    <div className={`form-group ${this.hasError('duration')}`}>
                        <label htmlFor="duration">Duration (No. of Days) <span className="required">*</span></label>
                        <input type="number" id="duration" name="duration" className="form-control border-input" onChange={ (e) => this.handleInput('duration', e) } />
                        <span className="help-block">{this.getError('duration')}</span>
                    </div>

                    <div className={`form-group ${this.hasError('amount')}`}>
                        <label htmlFor="amount">Total amount of package <span className="required">*</span></label>
                        <input type="number" step="any" id="amount" name="amount" className="form-control border-input" onChange={ (e) => this.handleInput('amount', e) } />
                        <span className="help-block">{this.getError('amount')}</span>
                    </div>

                    <div className={`form-group ${this.hasError('risk_profile')}`}>
                        <label htmlFor="risk_profile">Risk Profile <span className="required">*</span></label>
                        <select id="risk_profile" name="risk_profile" className="form-control border-input" onChange={ (e) => this.handleInput('risk_profile', e) }>
                            <option value="">Select Risk Profile</option>
                            <option value="conservative">Conservative</option>
                            <option value="moderate">Moderate</option>
                            <option value="aggressive">Aggressive</option>
                            <option value="risky">Risky</option>
                            <option value="high-risk">High Risk</option>
                        </select>

                        <span className="help-block">{this.getError('risk_profile')}</span>
                    </div>

                    <div className={`form-group ${this.hasError('frequency')}`}>
                        <label htmlFor="frequency">Frequency <span className="required">*</span></label>
                        <select id="frequency" name="frequency" className="form-control border-input" onChange={ (e) => this.handleInput('frequency', e) }>
                            <option value="">Select Frequency</option>
                            <option value="daily">Daily</option>
                            <option value="weekly">Weekly</option>
                            <option value="monthly">Monthly</option>
                        </select>

                        <span className="help-block">{this.getError('frequency')}</span>
                    </div>

                    <div className="form-group text-center">
                        <button type="submit" className="btn btn-primary btn-wd">Submit</button>
                    </div>
                </form>
            </Modal>
        );
    }
}

export default AddPackage;
