import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import queryString from 'query-string';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import ShowPackage from './ShowPackage';
import AddPackage from './AddPackage';
import EditPackage from './EditPackage';
import SearchPackage from './SearchPackage';
import Modal from '../components/Modal';
import Pagination from '../components/Pagination';
import Package from './partials/Package';

/* Main Component */
class Main extends Component {
    constructor() {
        super();
        // Initialize the state in the constructor
        this.state = {
            url: '/administrator/packages',
            items: {},
            currentItem: null,
            pagination: {},
            filter: {
                name: '',
                status: '',
                risk_profile: ''
            },
            show: false,
            loading: true,
            showPackageModal: false,
            showAddModal: false,
            showEditModal: false,
        };

        this.showEditModal          = this.showEditModal.bind(this);
        this.showAddModal           = this.showAddModal.bind(this);
        this.showPackageModal       = this.showPackageModal.bind(this);
        this.handleAddPackage       = this.handleAddPackage.bind(this);
        this.handleUpdatePackage    = this.handleUpdatePackage.bind(this);
        this.handleDeletePackage    = this.handleDeletePackage.bind(this);
        this.fetchPackages          = this.fetchPackages.bind(this);
        this.changePage             = this.changePage.bind(this);
        this.filterPackages         = this.filterPackages.bind(this);

        $('#loader').fadeIn(400);
    }

    /**
     * componentDidMount() is a lifecycle method
     * that gets called after the component is rendered
     */
    componentDidMount() {
        this.fetchPackages();

        setTimeout(() => {
            this.setState({ loading: false });
            $('#loader').fadeOut(400);
        }, 1000); // simulates loading of data
    }

    /**
     * Get packages
     */
    fetchPackages() {
        $('#loader').fadeIn(400);
        axios.get(`${this.state.url}?${this.getQueryParameters()}`)
        .then(response => {
            this.setState({
                items: response.data
            });

            this.makePagination(response.data);

            $('#loader').fadeOut(400);
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    /**
     * Get query parameters
     */
    getQueryParameters() {
        return queryString.stringify({
            page: this.state.pagination.current_page,
            name: this.state.filter.name,
            status: this.state.filter.status,
            risk_profile: this.state.filter.risk_profile,
        });
    }

    /**
     * Change current page
     */
    changePage(page) {
        // Duplicating and updating the state
        const pagination = Object.assign(this.state.pagination, {current_page: page});

        this.setState({
            pagination: pagination
        })

        this.fetchPackages();
    }

    /**
     * Filter packages
     */
    filterPackages(params) {
        // Duplicating and updating the state
        const filter = Object.assign(this.state.filter, params);

        this.setState({
            filter: filter
        })

        this.fetchPackages();
    }

    /**
     * Make pagination state
     */
    makePagination(data) {
        let pagination = {
            current_page: data.current_page,
            per_page: data.per_page,
            last_page: data.last_page,
            next_page_url: data.next_page_url,
            prev_page_url: data.prev_page_url,
            to: data.to,
            total: data.total,
        }

        this.setState({
            pagination: pagination
        })
    }

    /**
     * Show modal for package information
     */
    showPackageModal(item) {
        this.setState({ currentItem: item });

        this.setState({ showPackageModal: !this.state.showPackageModal });

        $('#show-package').modal(); // Convert to vanilla JS!
    }

    /**
     * Show modal for editing package information
     */
    showEditModal(item) {
        this.setState({ currentItem: item });

        this.setState({ showEditModal: !this.state.showEditModal });

        $('#edit-package').modal(); // Convert to vanilla JS!
    }

    /**
     * Show modal for adding a new package
     */
    showAddModal(){
        this.setState({ showAddModal: !this.state.showAddModal });

        $('#add-package').modal(); // Convert to vanilla JS!
    }

    handleAddPackage(item) {
        $('#loader').fadeIn(400);

        this.fetchPackages();

        this.createNotification('Add Package', 'Package Successfully Added', 'success');

        $('#loader').fadeOut(400);
    }

    handleUpdatePackage(item) {
        $('#loader').fadeIn(400);

        const currentItem = this.state.currentItem;

        this.fetchPackages();

        this.createNotification('Update Package', 'Package Successfully Updated', 'success');

        $('#loader').fadeOut(400);
    }

    handleDeletePackage(item) {
        const currentItem = item;

        axios.delete('/administrator/packages/' + currentItem.id)
        .then((response) => {
            this.fetchPackages();

            this.createNotification('Delete Package', 'Package Successfully Deleted', 'success');

        })
        .catch(function (error) {
            console.log(error)
        });
    }

    createNotification(title, message, type) {
        switch (type) {
            case 'info':
                NotificationManager.info(message, 3000);
                break;
            case 'success':
                NotificationManager.success(message, title, 3000);
                break;
            case 'warning':
                NotificationManager.warning(message, title, 3000);
                break;
            case 'error':
                NotificationManager.error(message, title, 3000);
            break;
        }
    };

    round(number, precision) {
        let shift = function (number, precision, reverseShift) {
            if (reverseShift) {
                precision = -precision;
            }
            let numArray = ("" + number).split("e");
            return +(numArray[0] + "e" + (numArray[1] ? (+numArray[1] + precision) : precision));
        };
        return shift(Math.round(shift(number, precision, false)), precision, true);
    }

    renderPackages() {
        if (this.state.items.data instanceof Array && this.state.items.data.length){
            return this.state.items.data.map(item => {
                return (
                    <div key={ item.id } className="package-item col-sm-6 col-xs-12">
                        <div className="card">
                            <div className="header">
                                <h4 className="title clearfix">
                                    <span className="ti-package"></span> { item.name }

                                    <div className="btn-toolbar pull-right">
                                        <button className="btn btn-success btn-fill btn-sm" onClick={() => this.showPackageModal(item)}>
                                            <span className="ti-search"></span>
                                        </button>
                                        <button className="btn btn-info btn-fill btn-sm" onClick={() => this.showEditModal(item)}>
                                            <span className="ti-pencil"></span>
                                        </button>
                                        <button className="btn btn-danger btn-fill btn-sm" onClick={() => this.handleDeletePackage(item)}>
                                            <span className="ti-trash"></span>
                                        </button>
                                    </div>
                                </h4>
                            </div>
                            <div className="content">
                                <div className="panel panel-default">
                                    <ul className="list-group">
                                        <li className="list-group-item">
                                            Fund Status <span className="badge">$ { item.fund_status } ( % { this.round(item.fund_status / item.amount * 100, 2) } )</span>
                                            <div className="clearfix"></div>
                                            <div className="progress">
                                                <div className="progress-bar progress-bar-info" role="progressbar" aria-valuenow={ this.round(item.fund_status / item.amount * 100, 2) } aria-valuemin="0" aria-valuemax="100" style={{width: this.round(item.fund_status / item.amount * 100, 2) + "%" }}>
                                                    <span className="sr-only">{ this.round(item.fund_status / item.amount * 100, 2) }% Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="list-group-item">Category <span className="badge">{ item.category.name }</span></li>
                                        <li className="list-group-item">Status <span className={`badge ${item.status}`}>{ item.status }</span></li>
                                        <li className="list-group-item">Risk Profile <span className={`badge ${item.risk_profile}`}>{ item.risk_profile }</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            })
        } else {
            return (
                <div className="col-xs-12">
                    <div className="card">
                        <div className="content">
                            <div className="alert alert-warning alert-with-icon" data-notify="container">
                                <span data-notify="icon" className="ti-bell"></span>
                                <span data-notify="message"><strong>No packages found.</strong></span>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }

    render() {
        if (this.state.loading) {
            return null;
        }

        return (
            <div>
                <div className="row">
                    <div className="col-xs-12">
                        <div className="card">
                            <div className="header">
                                <h3 className="title clearfix">
                                    Package List

                                    <button className="btn btn-success btn-fill pull-right" onClick={() => this.showAddModal()}>
                                        Add Package
                                    </button>
                                </h3>
                            </div>
                            <div className="content"></div>
                        </div>
                    </div>

                    <div className="col-xs-12">
                        <SearchPackage filter={this.filterPackages} />
                    </div>

                    { this.renderPackages() }

                    <div className="col-xs-12 text-center">
                        <Pagination pagination={this.state.pagination} paginate={this.changePage} />
                    </div>
                </div>

                {/* Notification */}
                <NotificationContainer />

                {/* Show Package Modal */}
                <ShowPackage item={this.state.currentItem} onDelete={this.handleDeletePackage} show={this.state.showPackageModal} />

                {/* Add Package Modal */}
                <AddPackage onAdd={this.handleAddPackage} show={this.state.showAddModal} />

                {/* Edit Package Modal */}
                <EditPackage item={this.state.currentItem} onUpdate={this.handleUpdatePackage} show={this.state.showEditModal} />
            </div>
        )
    }
}

export default Main;

/**
 * The if statement is required so as to Render the component on pages that have a div with an ID of "root";
*/

if (document.getElementById('packages-root')) {
    ReactDOM.render(<Main />, document.getElementById('packages-root'));
}

