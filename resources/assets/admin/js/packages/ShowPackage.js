import React, { Component } from 'react';
import Modal from '../components/Modal';

class ShowPackage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modal: {
                id: 'show-package',
                title: 'Package Information'
            }
        }
    }

    render() {
        // If the props item is null, return Package does not exist
        if (!this.props.item) {
            return (
                <Modal id={this.state.modal.id} title={this.state.modal.title} >
                    <div className="text-center">
                        <strong className="text-info">Please select a package.</strong>
                    </div>
                </Modal>
            )
        }

        const { item } = this.props;

        // Else, display the package data
        return (
            <Modal id={this.state.modal.id} title={this.state.modal.title} >
                <div className="content">
                    <div className="nav-tabs-navigation">
                        <div className="nav-tabs-wrapper">
                            <ul className="nav nav-tabs" data-tabs="tabs">
                                <li className="active"><a href="#basic-information" data-toggle="tab" aria-expanded="true">Basic Information</a></li>
                                <li className=""><a href="#investors" data-toggle="tab" aria-expanded="false">Investors</a></li>
                                <li className=""><a href="#profits" data-toggle="tab" aria-expanded="false">Profits</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="tab-content text-center">
                        <div id="basic-information" className="tab-pane active">
                            <ul className="list-group text-left">
                                <li className="list-group-item">
                                    <strong>Package Name: </strong>
                                    <strong className="pull-right">{ item.name }</strong>
                                </li>
                            </ul>
                            <ul className="list-group text-left">
                                <li className="list-group-item">
                                    <strong>Category:</strong>
                                    <span className="pull-right">{ item.category.name }</span>
                                </li>
                                <li className="list-group-item">
                                    <strong>Status:</strong>
                                    <span className="pull-right">{ item.status }</span>
                                </li>
                                <li className="list-group-item">
                                    <strong>Risk Profile:</strong>
                                    <span className="pull-right">{ item.risk_profile }</span>
                                </li>
                                <li className="list-group-item">
                                    <strong>For Membership:</strong>
                                    <span className="pull-right">{ item.for_membership ? 'Yes' : 'No' }</span>
                                </li>
                                <li className="list-group-item">
                                    <strong>Fund Status:</strong>
                                    <span className="pull-right">$ { item.fund_status }</span>
                                </li>
                                <li className="list-group-item">
                                    <strong>Package Total Amount:</strong>
                                    <span className="pull-right">$ { item.amount }</span>
                                </li>
                            </ul>
                            <ul className="list-group text-left">
                                <li className="list-group-item">
                                    <strong>Start Date: </strong>
                                    <span className="pull-right">{ item.start_date ? item.start_date : 'Pending'  }</span>
                                </li>
                                <li className="list-group-item">
                                    <strong>End Date: </strong>
                                    <span className="pull-right">{ item.end_date ? item.end_date : 'Pending'  }</span>
                                </li>
                            </ul>
                        </div>
                        <div id="investors" className="tab-pane">
                            <p>Content coming soon...</p>
                        </div>
                        <div id="profits" className="tab-pane">
                            <p>Content coming soon...</p>
                        </div>
                    </div>
                </div>
            </Modal>
        )
    }
}

export default ShowPackage;
