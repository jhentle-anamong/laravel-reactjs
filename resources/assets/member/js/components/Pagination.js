import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

class Pagination extends Component {
    constructor(props) {
        super(props);

        this.state = {
            url: '',
            pagination: {},
            offset: 2
        };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if ( ! _.isEqual(nextProps, prevState) ) {
            return {
                url: nextProps.url,
                pagination: nextProps.pagination
            }
        }

        // No state update necessary
        return null;
    }

    pagesNumber() {
        const { pagination, offset } = this.state;

        // if (!pagination.to) {
        //     return [];
        // }

        let from = pagination.current_page - offset;

        if (from < 1) {
            from = 1;
        }

        let to = from + (offset * 2);

        if (to >= pagination.last_page) {
            to = pagination.last_page;
        }

        let pagesArray = [];

        for (let page = from; page <= to; page++) {
            pagesArray.push(page);
        }

        return pagesArray;
    }

    renderPages() {
        let pages = this.pagesNumber();

        const { url } = this.state;

        return pages.map(page => {
            return (
                <li key={page} className={this.state.pagination.current_page == page ? 'active' : ''}>
                    <Link to={'/' + url + '/page/' + page}>{ page }</Link>
                </li>
            )
        });

    }


    render() {
        const { pagination, url } = this.state;

        let prevPage = pagination.current_page - 1,
            nextPage = pagination.current_page + 1;

        return (
            <ul className="pagination">
                <li className={pagination.current_page <= 1 ? 'disabled' : ''}>
                    <Link to={'/' + url + '/page/' + prevPage}>
                        <i className="fa fa-angle-double-left" aria-hidden="true"></i>
                    </Link>
                </li>

                { this.renderPages() }

                <li className={pagination.current_page >= pagination.last_page ? 'disabled' : ''}>
                    <Link to={'/' + url + '/page/' + nextPage}>
                        <i className="fa fa-angle-double-right" aria-hidden="true"></i>
                    </Link>
                </li>
            </ul>
        );
    }
}

Pagination.propTypes = {
    url: PropTypes.string.isRequired,
    pagination: PropTypes.object.isRequired,
}


export default Pagination;
