import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch, NavLink, Link } from "react-router-dom";
import PackageList from './PackageList';
import PackageDetails from './PackageDetails';

export default class Main extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path="/packages" component={PackageList} />
                    <Route path="/packages/page/:page" component={PackageList} />
                    <Route path="/packages/:id" component={PackageDetails} />
                </Switch>
            </Router>
        );
    }
}

if (document.getElementById('packages-root')) {
    ReactDOM.render(<Main />, document.getElementById('packages-root'));
}
