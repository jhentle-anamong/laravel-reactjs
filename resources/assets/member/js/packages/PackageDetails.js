import React, { Component } from 'react';
import { Link } from "react-router-dom";

class PackageDetails extends Component {
    constructor() {
        super();

        this.state = {
            url: '/packages/',
            item: {},
            needsUpdate: false,
            loading: true
        }

        this.fetchPackage = this.fetchPackage.bind(this);
    }

    componentDidMount() {
        this.fetchPackage();

        setTimeout(() => {
            this.setState({ loading: false });
            $('#loader').fadeOut(400);
        }, 1000); // simulates loading of data
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.needsUpdate) {
            this.setState({ needsUpdate: false, loading: false});
            this.fetchPackage();
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if ( nextProps.match.params.id != prevState.item.id ) {
            return {
                needsUpdate: true,
                loading: true
            }
        }

        // No state update necessary
        return null;
    }

    /**
     * Get package
     */
    fetchPackage() {
        $('#loader').fadeIn(400);
        axios.get(`${this.state.url + this.props.match.params.id}`)
        .then(response => {
            this.setState({
                item: response.data
            });

            $('#loader').fadeOut(400);
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    round(number, precision) {
        let shift = function (number, precision, reverseShift) {
            if (reverseShift) {
                precision = -precision;
            }
            let numArray = ("" + number).split("e");
            return +(numArray[0] + "e" + (numArray[1] ? (+numArray[1] + precision) : precision));
        };
        return shift(Math.round(shift(number, precision, false)), precision, true);
    }

    render() {
        const { item, loading } = this.state;

        if (loading) {
            return (
                null
            )
        }

        return(
            <div className="col-xs-12">
                <div className="card">
                    <div className="header">
                        <h3 className="title">
                            { item.name }

                            <div className="pull-right">
                                <Link to="/packages">
                                    <button className="btn btn-default btn-fill btn-wd">Package List</button>
                                </Link>
                            </div>
                        </h3>
                    </div>
                    <div className="content"></div>
                </div>
                <div className="card">
                    <div className="content">
                        <div className="nav-tabs-navigation">
                            <div className="nav-tabs-wrapper">
                                <ul className="nav nav-tabs" data-tabs="tabs">
                                    <li className="active"><a href="#basic-information" data-toggle="tab" aria-expanded="true">Basic Information</a></li>
                                    <li className=""><a href="#investors" data-toggle="tab" aria-expanded="false">Investors</a></li>
                                    <li className=""><a href="#profits" data-toggle="tab" aria-expanded="false">Profits</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="tab-content text-center">
                            <div id="basic-information" className="tab-pane active">
                                <ul className="list-group text-left">
                                    <li className="list-group-item">
                                        <strong>Package Name: </strong>
                                        <strong className="pull-right">{ item.name }</strong>
                                    </li>
                                </ul>
                                <ul className="list-group text-left">
                                    <li className="list-group-item">
                                        <strong>Category:</strong>
                                        <span className="pull-right">{ item.category.name }</span>
                                    </li>
                                    <li className="list-group-item">
                                        <strong>Status:</strong>
                                        <span className="pull-right">{ item.status }</span>
                                    </li>
                                    <li className="list-group-item">
                                        <strong>Risk Profile:</strong>
                                        <span className="pull-right">{ item.risk_profile }</span>
                                    </li>
                                    <li className="list-group-item">
                                        <strong>For Membership:</strong>
                                        <span className="pull-right">{ item.for_membership ? 'Yes' : 'No' }</span>
                                    </li>
                                    <li className="list-group-item">
                                        <strong>Fund Status:</strong>
                                        <span className="pull-right">$ { item.fund_status }</span>
                                    </li>
                                    <li className="list-group-item">
                                        <strong>Package Total Amount:</strong>
                                        <span className="pull-right">$ { item.amount }</span>
                                    </li>
                                </ul>
                                <ul className="list-group text-left">
                                    <li className="list-group-item">
                                        <strong>Start Date: </strong>
                                        <span className="pull-right">{ item.start_date ? item.start_date : 'Pending'  }</span>
                                    </li>
                                    <li className="list-group-item">
                                        <strong>End Date: </strong>
                                        <span className="pull-right">{ item.end_date ? item.end_date : 'Pending'  }</span>
                                    </li>
                                </ul>
                            </div>
                            <div id="investors" className="tab-pane">
                                <p>Content coming soon...</p>
                            </div>
                            <div id="profits" className="tab-pane">
                                <p>Content coming soon...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default PackageDetails;
