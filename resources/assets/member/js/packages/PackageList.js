import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import queryString from 'query-string';
import Pagination from '../components/Pagination';
import PackageItem from './PackageItem';

/* PackageList Component */
class PackageList extends Component {
    constructor() {
        super();
        // Initialize the state in the constructor
        this.state = {
            url: '/packages',
            items: {},
            pagination: {},
            loading: true,
            needsUpdate: false,
        };

        this.fetchPackages = this.fetchPackages.bind(this);

        $('#loader').fadeIn(400);
    }

    componentDidMount() {
        this.fetchPackages();

        setTimeout(() => {
            this.setState({ loading: false });
            $('#loader').fadeOut(400);
        }, 1000); // simulates loading of data
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.needsUpdate) {
            this.setState({ needsUpdate: false, loading: false});
            this.fetchPackages();
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if ( nextProps.match.params.page != prevState.pagination.current_page ) {
            return {
                loading: true,
                pagination: {
                    current_page: nextProps.match.params.page
                },
                needsUpdate: true
            }
        }

        // No state update necessary
        return null;
    }

    /**
     * Get packages
     */
    fetchPackages() {
        $('#loader').fadeIn(400);
        axios.get(`${this.state.url}?${this.getQueryParameters()}`)
        .then(response => {
            this.setState({
                items: response.data
            });

            this.makePagination(response.data);

            $('#loader').fadeOut(400);
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    /**
     * Get query parameters
     */
    getQueryParameters() {
        return queryString.stringify({
            // page: this.state.pagination.current_page
            page: this.props.match.params.page
        });
    }

    /**
     * Make pagination state
     */
    makePagination(data) {
        let pagination = {
            current_page: data.current_page,
            per_page: data.per_page,
            last_page: data.last_page,
            next_page_url: data.next_page_url,
            prev_page_url: data.prev_page_url,
            to: data.to,
            total: data.total,
        }

        this.setState({
            pagination: pagination
        })
    }

    renderPackages() {
        if (this.state.items.data instanceof Array && this.state.items.data.length){
            return this.state.items.data.map(item => {
                return (
                    <PackageItem key={ item.id } item={item} />
                );
            })
        } else {
            return (
                <div className="col-xs-12">
                    <div className="card">
                        <div className="content">
                            <div className="alert alert-warning alert-with-icon" data-notify="container">
                                <span data-notify="icon" className="ti-bell"></span>
                                <span data-notify="message"><strong>No packages found.</strong></span>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }

    render() {
        if (this.state.loading) {
            return null;
        }

        return (
            <div>
                <div className="row">
                    <div className="col-xs-12">
                        <div className="card">
                            <div className="header">
                                <h3 className="title">
                                    Package List
                                </h3>
                            </div>
                            <div className="content"></div>
                        </div>
                        <div className="row">
                            { this.renderPackages() }
                        </div>
                    </div>

                    <div className="col-xs-12 text-center">
                        <Pagination url="packages" pagination={this.state.pagination} />
                    </div>
                </div>
            </div>
        )
    }
}

export default PackageList;
