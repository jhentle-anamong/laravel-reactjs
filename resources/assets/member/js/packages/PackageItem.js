import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Package extends Component {
    constructor() {
        super();
    }

    round(number, precision) {
        let shift = function (number, precision, reverseShift) {
            if (reverseShift) {
                precision = -precision;
            }
            let numArray = ("" + number).split("e");
            return +(numArray[0] + "e" + (numArray[1] ? (+numArray[1] + precision) : precision));
        };
        return shift(Math.round(shift(number, precision, false)), precision, true);
    }

    render() {
        const { item } = this.props;

        return(
            <div key={ item.id } className="package-item col-xs-12">
                <div className="card">
                    <div className="header">
                        <h4 className="title clearfix">
                            <span className="ti-package"></span> { item.name }

                            <div className="btn-toolbar pull-right">
                                <Link to={'/packages/' + item.id}>
                                    <button className="btn btn-success btn-fill btn-sm">
                                        <span className="ti-search"></span>
                                    </button>
                                </Link>
                            </div>
                        </h4>
                    </div>
                    <div className="content">
                        <div className="panel panel-default">
                            <ul className="list-group">
                                <li className="list-group-item">
                                    Fund Status <span className="badge">$ { item.fund_status } ( % { this.round(item.fund_status / item.amount * 100, 2) } )</span>
                                    <div className="clearfix"></div>
                                    <div className="progress">
                                        <div className="progress-bar progress-bar-info" role="progressbar" aria-valuenow={ this.round(item.fund_status / item.amount * 100, 2) } aria-valuemin="0" aria-valuemax="100" style={{width: this.round(item.fund_status / item.amount * 100, 2) + "%" }}>
                                            <span className="sr-only">{ this.round(item.fund_status / item.amount * 100, 2) }% Complete</span>
                                        </div>
                                    </div>
                                </li>
                                <li className="list-group-item">Category <span className="badge">{ item.category.name }</span></li>
                                <li className="list-group-item">Status <span className={`badge ${item.status}`}>{ item.status }</span></li>
                                <li className="list-group-item">Risk Profile <span className={`badge ${item.risk_profile}`}>{ item.risk_profile }</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Package;
