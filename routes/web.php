<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');

Auth::routes();

Route::group(['namespace' => 'Member', 'middleware' => 'auth', 'as' => 'member.'], function () {
    Route::get('/dashboard', function () {
        return view('member.dashboard.index');
    })->name('dashboard');

    Route::get('/packages', 'PackagesController@index')->name('packages.index');
    Route::get('/packages/{package}', 'PackagesController@show')->name('packages.show');
    Route::get('/packages/page/{page}', 'PackagesController@index');
});

Route::group(['prefix' => 'administrator', 'namespace' => 'Admin', 'middleware' => ['auth', 'role:admin']], function () {
	Route::get('/', 'DashboardController@index')->name('admin.dashboard');

	// User Profile
	Route::get('/profiles/{user}', 'ProfilesController@show')->name('admin.profile');
});

Route::group(['prefix' => 'administrator', 'namespace' => 'Admin', 'middleware' => ['auth', 'role:super-admin']], function () {
    // Packages
    Route::resource('/packages', 'PackagesController');
});
