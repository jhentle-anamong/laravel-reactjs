<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Package::class, function (Faker $faker) {
	$amount 			= rand(1, 100000);
    $statuses           = ['available', 'funded-active', 'closed'];
    $risk_profiles      = ['conservative', 'moderate', 'aggressive', 'risky', 'high-risk'];
    $frequencies  		= ['daily', 'weekly', 'monthly'];
    $member_types       = ['platinum', 'gold', 'silver', 'free'];

    return [
        'category_id' 		=> rand(1, 3),
        'name'				=> $faker->name,
        'amount'			=> $amount,
        'status'			=> array_random($statuses),
        'duration'          => rand(75, 90),
        'fund_status'		=> rand(1, $amount),
        'for_membership' 	=> rand(0, 1),
        'risk_profile' 		=> array_random($risk_profiles),
        'frequency' 		=> array_random($frequencies)
    ];
});
