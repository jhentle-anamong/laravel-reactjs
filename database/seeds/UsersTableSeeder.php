<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            [
                'id'            => 1,
                'name'          => 'Jhentle Anamong',
                'email'         => 'jhentle.esilverconnect@gmail.com',
                'password'      => bcrypt('64853152'),
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ],
            [
                'id'            => 2,
                'name'          => 'John Doe',
                'email'         => 'john.doe@email.com',
                'password'      => bcrypt('password'),
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ],
            [
                'id'            => 3,
                'name'          => 'Jane Doe',
                'email'         => 'jane.doe@email.com',
                'password'      => bcrypt('password'),
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ]
        ]);
    }
}
