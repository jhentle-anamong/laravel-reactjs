<?php

use Illuminate\Database\Seeder;

class PackageCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('package_categories')->insert([
            [
                'id'            => 1,
                'name'          => 'Crypto Trading',
                'slug'          => 'crypto-trading',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ],
            [
                'id'            => 2,
                'name'          => 'Crypto Mining',
                'slug'          => 'crypto-mining',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ],
            [
                'id'            => 3,
                'name'          => 'Crypto Exchange',
                'slug'          => 'crypto-exchange',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ]
        ]);

    }
}
