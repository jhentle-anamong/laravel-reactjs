<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('permissions')->insert([
            [
                'id'            => 1,
                'name'          => 'View Users',
                'key'           => 'view-users',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ],
            [
                'id'            => 2,
                'name'          => 'Create Users',
                'key'           => 'create-users',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ],
            [
                'id'            => 3,
                'name'          => 'Update Users',
                'key'           => 'update-users',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ],
            [
                'id'            => 4,
                'name'          => 'Delete Users',
                'key'           => 'delete-users',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ],
            [
                'id'            => 5,
                'name'          => 'View packages',
                'key'           => 'view-packages',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ],
            [
                'id'            => 6,
                'name'          => 'Create Packages',
                'key'           => 'create-packages',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ],
            [
                'id'            => 7,
                'name'          => 'Update Packages',
                'key'           => 'update-packages',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ],
            [
                'id'            => 8,
                'name'          => 'Delete Packages',
                'key'           => 'delete-packages',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d')
            ]
        ]);
    }
}
