<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert([
            [
                'id'            => 1,
                'name'          => 'Super Admin',
                'key'           => 'super-admin',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ],
            [
                'id'            => 2,
                'name'          => 'Admin',
                'key'           => 'admin',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ],
            [
                'id'            => 3,
                'name'          => 'Member',
                'key'           => 'member',
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
            ]
        ]);
    }
}
