<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Ask for db migration refresh, default is no
        if ($this->command->confirm('Do you wish to refresh migration before seeding, it will clear all old data?')) {
            // Call the php artisan migrate:fresh
            $this->command->call('migrate:fresh');

            $this->command->warn("Data cleared, starting from blank database.");
        }

        // Disable all mass assignment restrictions
        Model::unguard();

        $this->call([
            UsersTableSeeder::class,
            RolesTableSeeder::class,
            PermissionsTableSeeder::class,
            UsersRolesTableSeeder::class,
            UsersPermissionsTableSeeder::class,
            PackagesTableSeeder::class,
            PackageCategoriesTableSeeder::class,
        ]);

        // Re enable all mass assignment restrictions
        Model::reguard();
    }
}
